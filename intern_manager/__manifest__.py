# -*- coding: utf-8 -*-
{
    'name': "Internship Manager",  # Module title
    'summary': "Manage projects for remote internships",  # Module subtitle phrase
    'description': """""",  # You can also rst format
    'author': "Darío Costa",
    # 'website': "http://www.example.com",
    'category': 'Uncategorized',
    'version': '12.0.1',
    'depends': ['base','mail'],
    # This data files will be loaded at the installation (commented becaues file is not added in this example)
    'data': [
        'security/groups.xml',
        'security/ir.model.access.csv',
        'views/projects.xml',
    ],
    # This demo data files will be loaded if db initialize with demo data (commented becaues file is not added in this example)
    # 'demo': [
    #     'data/demo_data.xml'
    # ],
    'application': True,
}
