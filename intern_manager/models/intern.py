# -*- coding: utf-8 -*-
from odoo import api, models, fields
from datetime import datetime

class Intern(models.Model):
    _name = 'intern_manager.intern'
    _description = 'Estudiante en prácticas'
    _inherits = {'res.users': 'user_id'}

    user_id = fields.Many2one('res.users', ondelete='cascade')

    centro_procedencia = fields.Char("Centro escolar del que proviene")
    inicio_internship = fields.Datetime("Fecha de inicio del período de prácticas", default=fields.Datetime.now)
    final_internship = fields.Datetime("Fecha de final del período de prácticas")

    id_supervisor = fields.Many2one('res.users', string='Supervisor')

class Project(models.Model):
    _name = "intern_manager.project"
    _description = "Proyecto"
    _inherit = ['mail.thread']
    _mail_post_access = 'read'
    _rec_name="name"

    fecha_limite = fields.Datetime("Fecha límite para la entrega del proyecto")
    name = fields.Char("Project Name", required=True)
    description = fields.Text("Project Description")

    task_ids = fields.One2many('intern_manager.task', 'project_id', string='Tareas')
    submission_ids = fields.One2many('intern_manager.submission', 'project_id', string='Entregas')

    task_count = fields.Integer(string="Count", compute='_compute_count')

    def _compute_count(self):
        task_data = self.env['intern_manager.task'].read_group([('project_id', 'in', self.ids)], ['project_id'], ['project_id'])
        result = dict((data['project_id'][0], data['project_id_count']) for data in task_data)
        for project in self:
            project.task_count = result.get(project.id, 0)

class Task(models.Model):
    _name = 'intern_manager.task'
    _description = 'Tarea'
    _inherit = ['mail.thread']
    _rec_name="name"
    _mail_post_access = 'read'

    project_id = fields.Many2one('intern_manager.project',
        string='Project',
        default=lambda self: self.env.context.get('default_project_id'),required=True)
    name = fields.Char("Task Name", required=True)
    description = fields.Text("Task Description")

class Submission(models.Model):
    _name = 'intern_manager.submission'
    _description = 'Entrega'
    _rec_name="enlace_externo"
    _inherit = ['mail.thread']
    _mail_post_access = 'read'
    _rec_name="enlace_externo"

    project_id = fields.Many2one('intern_manager.project',
        string='Project',
        default=lambda self: self.env.context.get('default_project_id'),required=True)

    submitter = fields.Many2one('res.users',
        default=lambda self: self.env.user,required=True)

    enlace_externo = fields.Char("Link repositorio entrega", required=True)

    fecha_entrega = fields.Datetime("Fecha de inicio del período de prácticas", default=fields.Datetime.now)
