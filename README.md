# Internship Manager

## Descripción y motivación

Internship Manager (en español: Administrador de prácticas en empresas) es un módulo para Odoo que propone facilitar la realización y adminstración de prácticas en empresas de forma remota, algo que está sucediendo recientemente debido al Covid-19.

## Funcionalidades y código relevante

### Gestión de alumnos en prácticas
La gestión de los alumnos en prácticas y toda su información se puede realizar desde la pestaña "Alumnos en prácticas"

![](readme_files/interns.png)

En código, se trata de un Modelo que hereda de `res.users` y añade información adicional, como el supervisor de cada alumno.

![](readme_files/interns_code.png)

### Gestión de proyectos

Cada proyecto contiene una lista de tareas (`Task`) y entregas (`Submission`).

Las tareas representan los diferentes pasos que llevar a cabo por cada alumno en prácticas para terminar el proyecto. Los alumnos podrán publicar dudas o preguntas en cada tarea.

Las entregas son realizadas por el alumno en prácticas cuando terminen el proyecto.

En la pestaña Proyectos se accede a toda la información de cada proyecto.

En la `form view` de cada proyecto se muestran (en la esquina superior derecha) el número de tareas y entregas. Se puede hacer click para acceder a la vista kanban de cada uno.

![](readme_files/project.png)

Para conocer el número de tareas se usa un `field` calculado. En la función donde se calcula se hace una cuenta del número de modelos `Task` que tienen el `project_id` correspondiente:
![](readme_files/task_count_code.png)

Los administradores pueden acceder a todas las entregas, pero cada usuario normal sólo puede ver sus propias entregas y no las de los otros alumnos. Para conseguir esto, se usaron varias reglas de privacidad específicas:

![](readme_files/rules.png)


### Uso de `mail.thread`

![](readme_files/message1.gif)

Varios modelos heredan de `mail.thread` para añadir funcionalidad de mensajería:
- `Project`, de forma que los alumnos puedan postear preguntas generales sobre el proyecto.
- `Task`, de forma que los alumnos puedan postear dudas específicas durante la realización de la tarea.
- `Submission`,  de forma que el supervisor puede evaluar y añadir _feedback_ a cada entrega realizada.

![](readme_files/message2.gif)

Se puede además añadir archivos a las tareas o proyectos:

![](readme_files/attach.gif)


En código, cabe destacar la modificación del campo heredado `_mail_post_access = 'read'`, que permite que cada alumno pueda publicar mensajes aunque no tengan acceso de escritura a los módulos con dicha funcionalidad.
![](readme_files/task.png)

## Calendar

Se incluye una vista calendario que permite visualizar cada proyecto y su fecha de entrega.

![](readme_files/calendar.png)